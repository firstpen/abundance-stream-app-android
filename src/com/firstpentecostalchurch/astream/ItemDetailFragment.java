package com.firstpentecostalchurch.astream;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.firstpentecostalchurch.astream.menu.MenuContent;
import com.firstpentecostalchurch.astream.ItemListActivity;


/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
@SuppressLint("SetJavaScriptEnabled")
public class ItemDetailFragment extends Fragment {

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	public static final String Site = "site_address";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private MenuContent.MenuItem mItem;
	private String wSite;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	       
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = MenuContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
			wSite = getArguments().getString(Site);
			getActivity().getActionBar().setTitle(mItem.content);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);


		
		// Show item content in a WebView.
			final WebView webview = (WebView) rootView
		            .findViewById(R.id.item_detail);
			WebSettings webSettings = webview.getSettings();
			webSettings.setJavaScriptEnabled(true);
			webview.getSettings().setSupportZoom(true);
			webview.getSettings().setBuiltInZoomControls(true);
		    webview.setWebViewClient(new WebViewClient()
		    {
	            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
	                webview.loadUrl("file:///android_asset/myerrorpage.html");

	            }});
		    webview.loadUrl(wSite);
		    return rootView;

	}
	
}
