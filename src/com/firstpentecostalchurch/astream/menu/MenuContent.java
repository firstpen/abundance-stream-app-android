package com.firstpentecostalchurch.astream.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing content for user interfaces.
 */
public class MenuContent {

	/**
	 * An array of menu items.
	 */
	public static List<MenuItem> ITEMS = new ArrayList<MenuItem>();

	/**
	 * A map of menu items, by ID.
	 */
	public static Map<String, MenuItem> ITEM_MAP = new HashMap<String, MenuItem>();

	static {
		// Add menu items.
		addItem(new MenuItem("1", "Conference Registration"));
		addItem(new MenuItem("2", "Conference Event Schedule"));
		addItem(new MenuItem("3", "Webcasts & Live Streams"));
		addItem(new MenuItem("4", "Help & Instructions"));
		addItem(new MenuItem("5", "Feedback & Support"));
	}

	private static void addItem(MenuItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	/**
	 * A menu item representing navigation content.
	 */
	public static class MenuItem {
		public String id;
		public String content;

		public MenuItem(String id, String content) {
			this.id = id;
			this.content = content;
		}

		@Override
		public String toString() {
			return content;
		}
	}
}
